module Supply where

import State

newtype Supply s a = S (State [s] a)

runSupply :: Supply s a -> [s] -> (a, [s])
runSupply (S m) xs = runState m xs

next :: Supply s (Maybe s)
next = S $
       get >>= \st ->
       case st of
         [] -> return Nothing
         (x:xs) ->
           put xs >>
           return (Just x)
  
unwrapS :: Supply s a -> State [s] a
unwrapS (S s) = s

instance Monad (Supply s) where
  s >>= m = S (unwrapS s >>= unwrapS . m)
  return = S . return
