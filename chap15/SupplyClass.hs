
class (Monad m) => MonadSupply s m | m -> s where
  next :: m (Maybe s)
