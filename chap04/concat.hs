myConcat xs = foldr step [] xs
  where step x sum = x ++ sum

myConcat2 xs = foldl step [] xs
  where step sum x = sum ++ x
