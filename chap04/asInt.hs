import Data.Char


asInt_fold ('-':xs) = (-1) * asInt_fold xs
asInt_fold xs       = foldl step 0 xs
  where step acc x = acc * 10 + digitToInt x

type ErrorMessage = String
asInt_either :: String -> Either ErrorMessage Int
asInt_either xs | head xs == '-' = result (-1) (foldl step (Right 0) (tail xs))
                | otherwise      = result   1  (foldl step (Right 0) xs)
                                   
  where result sign (Right n)    = Right (sign * n)
        result _    (Left error) = Left error
        
        step (Right acc) x  = if x >= '0' && x <= '9'
                              then Right (acc * 10 + digitToInt x)
                              else Left ("non-digit " ++ (show x))
        step (Left error) _ = Left error
