myTakeWhile _ []     = []
myTakeWhile f (x:xs) = if f x
                       then x : myTakeWhile f xs
                       else []

myTakeWhile_foldr f xs = foldr step [] xs
  where step x list = if f x
                      then x : list
                      else []
