myGroupBy f xs = foldr step [[]] xs
  where step x [[]] = [[x]]
        step x sum  = if f x y
                      then [x : (head sum)] ++ tail sum
                      else [[x]] ++ sum
          where y = head (head sum)
