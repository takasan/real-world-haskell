
safeHead []     = Nothing
safeHead xs = Just (head xs)

safeTail [] = Nothing
safeTail xs = Just (tail xs)

safeLast [] = Nothing
safeLast xs = Just (last xs)

safeInit [] = Nothing
safeInit xs = Just (init xs)
              
--
chars []     = []
chars (x:xs) = [x] : chars xs

swapRowColumn str = map (\x -> x ++ "\n") (swap list)
  where list = lines str

swap (x:[])   = chars x
swap (x:y:[]) = myZipWith (charConcat) (charOne) x y
swap (x:y:xs) = myZipWith (++) id t (swap xs)
  where t = myZipWith (charConcat) (charOne) x y

charConcat :: Char -> Char -> [Char]
charConcat a b = a : b : []

charOne a = [a]

myZipWith _ _   []     []     = []
myZipWith f one []     (y:ys) = one y   : (myZipWith f one [] ys)
myZipWith f one (x:xs) []     = one x   : (myZipWith f one xs [])
myZipWith f one (x:xs) (y:ys) = x `f` y : (myZipWith f one xs ys)

--
myAny f xs = foldr step False xs
  where step x False = f x
        step x True  = True

myCycle xs = foldr step [] [1..]
  where step x sum = xs ++ sum

myWords xs = foldr step [[]] xs
  where step x [[]] | x `elem` [' ','\r','\n'] = [[]]
                    | otherwise                = [[x]]
        step x sum  = if x `elem` [' ','\r','\n']
                      then if length (head sum) /= 0
                           then [[]] ++ sum
                           else sum
                      else [x : head sum] ++ tail sum

myUnlines xs = foldr step "" xs
  where step x sum = (x ++ "\n") ++ sum
