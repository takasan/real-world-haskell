isParindrome :: (Eq a) => [a] -> Bool

isParindrome [] = True
isParindrome (x:[]) = True
isParindrome (x:xs) = if x == (last xs)
                      then isParindrome (take ((length xs) - 1) xs)
                      else False
