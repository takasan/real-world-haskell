data Tree a = Node a (Tree a) (Tree a)
            | Empty
            deriving (Show)

depth :: Tree a -> Int
depth Empty = 0
depth (Node _ r l) = max (1 + (depth r)) (1 + (depth l))
