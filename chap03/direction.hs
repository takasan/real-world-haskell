data Direction = Left
               | Right
               | Straight
                 deriving (Show)

data Point2D = Point2D {
  x :: Double,
  y :: Double
  } deriving (Show)

directions [] = []
directions xs | length xs < 3 = []
              | length xs == 3 = [direction first second third]
              | length xs > 3 = [direction first second third] ++ directions rest
  where first = xs !! 0
        second = xs !! 1
        third = xs !! 2
        rest = drop 1 xs

direction a b c = dir c_b a_b
  where a_b = Point2D {x = (x a) - (x b), y = (y a) - (y b)}
        c_b = Point2D {x = (x c) - (x b), y = (y c) - (y b)}

dir a b = if angle > 0
           then Main.Left
           else if angle < 0
                then Main.Right
                else Main.Straight
  where angle = cross a b
             
cross :: Point2D -> Point2D -> Double
cross a b = (x a) * (y b) - (y a) * (x b)
