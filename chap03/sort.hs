import Data.List

sort list = sortBy comp list

comp lst1 lst2 = if (length lst1) < (length lst2)
                 then LT
                 else if (length lst1) == (length lst2)
                        then EQ
                        else GT
