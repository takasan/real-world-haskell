
newtype MaybeT m a = MaybeT {
  runMaybeT :: m (Maybe a)
  }

--bindMT :: (Monad m) => MaybeT m a -> (a -> MaybeT m b) -> MaybeT m b
bindMT m f = MaybeT $
  r >>= \r' ->
  case r' of
    (Just x) -> runMaybeT $ f x
  where r = runMaybeT m
