module Parse where

import PNM
import Control.Applicative
import qualified Data.ByteString.Lazy.Char8 as L8
import qualified Data.ByteString.Lazy as L
import Data.Int
import Data.Word
import Data.Char

data ParseState = ParseState {
    string :: L.ByteString
  , offset :: Int64
  } deriving (Show)

newtype Parse a = Parse {
  runParse :: ParseState -> Either String (a, ParseState)
  }

identify :: a -> Parse a
identify a = Parse (\s -> Right (a, s))

parse :: Parse a -> L.ByteString -> Either String a
parse parser initState
  = case runParse parser (ParseState initState 0) of
    Left err -> Left err
    Right (result, _) -> Right result

modifyOffset :: ParseState -> Int64 -> ParseState
modifyOffset initState newOffset = initState { offset = newOffset }

parseByte :: Parse Word8
parseByte = getState ==> \initState ->
  case L.uncons (string initState) of
    Nothing ->
      bail "no more input"
    Just (byte, remainder) ->
      putState newState ==> \_ ->
      identify byte
      where newState = initState { string = remainder,
                                   offset = newOffset }
            newOffset = offset initState + 1

getState :: Parse ParseState
getState = Parse (\s -> Right (s, s))

putState :: ParseState -> Parse ()
putState s = Parse (\_ -> Right ((), s))

bail :: String -> Parse a
bail err = Parse $ \s -> Left $
                         "byte offset " ++ show (offset s) ++ ": " ++ err

(==>) :: Parse a -> (a -> Parse b) -> Parse b
firstParser ==> secondParser = Parse chainedParser
  where chainedParser initState =
          case runParse firstParser initState of
            Left errMessage ->
              Left errMessage
            Right (firstResult, newState) ->
              runParse (secondParser firstResult) newState

instance Functor Parse where
  fmap f parser = parser ==> \result ->
    identify (f result)

w2c :: Word8 -> Char
w2c = chr . fromIntegral

parseChar :: Parse Char
parseChar = w2c <$> parseByte

peekByte :: Parse (Maybe Word8)
peekByte = (fmap fst . L.uncons . string) <$> getState

peekChar :: Parse (Maybe Char)
peekChar = fmap w2c <$> peekByte

parseWhile :: (Word8 -> Bool) -> Parse [Word8]
parseWhile p = (fmap p <$> peekByte) ==> \mp ->
  if mp == Just True
  then parseByte ==> \b ->
       (b:) <$> parseWhile p
  else identify []

parseRawPGM =
  parseWhileWith w2c notWhite ==> \header -> skipSpaces ==>&
  assert (header == "P5") "invalid raw header" ==>&
  parseNat ==> \width -> skipSpaces ==>&
  parseNat ==> \height -> skipSpaces ==>&
  parseNat ==> \maxGrey ->
  parseByte ==>&
  parseBytes (width * height) ==> \bitmap ->
  identify (Greymap width height maxGrey bitmap)

parseWhileWith :: (Word8 -> a) -> (a -> Bool) -> Parse [a]
parseWhileWith f p = fmap f <$> parseWhile (p . f)

parseNat :: Parse Int
parseNat = parseWhileWith w2c isDigit ==> \digits ->
  if null digits
  then bail "no more input"
  else let n = read digits
       in if n < 0
          then bail "integer overflow"
          else identify n

(==>&) :: Parse a -> Parse b -> Parse b
p ==>& f = p ==> \_ -> f

skipSpaces :: Parse ()
skipSpaces = parseWhileWith w2c isSpace ==>& identify ()

assert :: Bool -> String -> Parse ()
assert True   _   = identify ()
assert False err = bail err

parseBytes :: Int -> Parse L.ByteString
parseBytes n =
  getState ==> \st ->
  let n' = fromIntegral n
      (h, t) = L.splitAt n' (string st)
      st' = st { offset = offset st + L.length h, string = t }
  in putState st' ==>&
     assert (L.length h == n') "end of input" ==>&
     identify h

parsePlainPGM =
  parseWhileWith w2c notWhite ==> \header -> skipSpaces ==>&
  assert (header == "P2") "invalid plain header" ==>&
  parseNat ==> \width -> skipSpaces ==>&
  parseNat ==> \height -> skipSpaces ==>&
  parseNat ==> \maxGrey ->
  parseLoop (width * height) (skipSpaces ==>& parseNat) ==> \d ->
  let chrmap = map chr d
      bitmap = L8.pack chrmap
  in identify (Greymap width height maxGrey bitmap)

parseInt :: Parse Int
parseInt = getState ==> \initState ->
  case L8.readInt (string initState) of
    Nothing ->
      bail "no more input"
    Just (int, remainder) ->
      putState newState ==> \_ ->
      identify int
      where newState = initState { string = remainder,
                                   offset = newOffset }
            newOffset = offset initState +
                        ((L8.length $ string initState) -
                         (L8.length remainder))

parseLoop :: Int -> Parse a -> Parse [a]
parseLoop 0 _      = identify []
parseLoop n parser = parser ==> \num ->
  (num:) <$> parseLoop (n - 1) parser

parseRawPGM_2byte =
  parseWhileWith w2c notWhite ==> \header -> skipSpaces ==>&
  assert (header == "P5") "invalid raw header" ==>&
  parseNat ==> \width  -> skipSpaces ==>&
  parseNat ==> \height -> skipSpaces ==>&
  parseNat ==> \maxGrey ->
  parseByte ==>&
  let parser =
         if maxGrey <= 255
         then parseBytes (width * height)
         else parseBytes (width * height * 2)
  in parser ==> \bitmap ->
  identify (Greymap width height maxGrey bitmap)

parsePGM =
  peekHeader ==> \header ->
  let parser = if header == "P5"
               then parseRawPGM_2byte
               else parsePlainPGM
  in parser

peekHeader =
  getState ==> \state ->
  parseWhileWith w2c notWhite ==> \header ->
  putState state ==> \_ ->
  identify header

notWhite = (`notElem` " \r\n\t")
