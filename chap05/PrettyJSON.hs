module PrettyJSON
       (
         renderJValue
       ) where

import Numeric (showHex)
import Data.Char (ord)
import Data.Bits (shiftR, (.&.))  
import SimpleJSON (JValue(..))
import Prettify

renderJValue :: JValue -> Doc
renderJValue (JBool True) = text "true"
renderJValue (JBool False) = text "false"
renderJValue JNull = text "null"
renderJValue (JNumber num) = double num
renderJValue (JString str) = string str
renderJValue (JArray ary) = series '[' ']' renderJValue ary
renderJValue (JObject obj) = series '{' '}' field obj
  where field (name,val) = string name
                           <> text ": "
                           <> renderJValue val

string = enclose '"' '"' . hcat . map oneChar

enclose left right x = char left <> x <> char right

oneChar c = case lookup c simpleEscapes of
  Just r -> text r
  Nothing | mustEscape c -> hexEscape c
          | otherwise -> char c
  where mustEscape c = c < ' ' || c == '\x7f' || c > '\xff'

simpleEscapes = zipWith ch "\b\n\f\r\\\"/" "bnfr\\\"/"
  where ch a b = (a, ['\\', b])

smallHex x = text "\\u"
             <> text (replicate (4 - length h) '0')
             <> text h
  where h = showHex x ""

astral n = smallHex (a + 0xd800) <> smallHex (b + 0xdc00)
  where a = (n `shiftR` 10) .&. 0x3ff
        b = n .&. 0x3ff

hexEscape c | d < 0xd10000 = smallHex d
            | otherwise = astral (d - 0x10000)
  where d = ord c

series open close item = enclose open close
                         . fsep . punctuate (char ',') . map item

punctuate p [] = []
punctuate p [d] = [d]
punctuate p (d:ds) = (d <> p) : punctuate p ds

compat x = transform [x]
  where transform [] = ""
        transform (d:ds) =
          case d of
            Empty -> transform ds
            Char c -> c : transform ds
            Text s -> s ++ transform ds
            Line -> '\n' : transform ds
            a `Concat` b -> transform (a:b:ds)
            _ `Union` b -> transform (b:ds)

pretty width x = best 0 [x]
  where best col (d:ds) =
          case d of
            Empty -> best col ds
            Char c -> c : best (col + 1) ds
            Text s -> s ++ best (col + length s) ds
            Line -> '\n' : best 0 ds
            a `Concat` b -> best col (a:b:ds)
            a `Union` b -> nicest col (best col (a:ds))
                                      (best col (b:ds))
        best _ _ = ""
        nicest col a b | (width - least) `fits` a = a
                       | otherwise = b
          where least = min width col

w `fits` _ | w < 0 = False
w `fits` "" = True
w `fits` ('\n':_) = True
w `fits` (c:cs) = (w - 1) `fits` cs
                  
fill width x = text $ fill' 0 [x]
  where fill' col (d:ds) =
          case d of
            Empty -> fill' col ds
            Char c -> c : fill' (col + 1) ds
            Text s -> s ++ fill' (col + length s) ds
            Line -> padding col ++ "\n" ++ fill' 0 ds
            a `Concat` b -> fill' col (a:b:ds)
            a `Union` b -> fill' col (b:ds)
        fill' col [] = ""

        padding col = take (width - col) (repeat ' ')

nest x = text $ nest' 0 [x]
  where nest' level (d:ds) =
          case d of
            Empty -> nest' level ds
            Char '{' -> '{' : nest' (level+2) (Line:ds)
            Char '}' -> '}' : nest' (level-2) ds
            Char '[' -> '[' : nest' (level+2) (Line:ds)
            Char ']' -> ']' : nest' (level-2) ds
            Char c -> c : nest' level ds
            Text s -> s ++ nest' level ds
            Line -> if (head $ dropWhile (isSpace) (nest' level ds)) == '}'
                    then "\n" ++ indentation (level-2) ++ nest' level ds
                    else "\n" ++ indentation level ++ nest' level ds
            a `Concat` b -> nest' level (a:b:ds)
            a `Union` b -> nest' level (b:ds)
        nest' col [] = ""

        indentation level = take level (repeat ' ')

isSpace c = c == ' '
