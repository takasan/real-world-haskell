module PrettyStub where

import SimpleJSON

data Doc = ToBeDefined
           deriving (Show)

a <> b = undefined

char c = undefined

hcat xs = undefined

oneChar char = undefined

text str = undefined

double num = undefined
