module Prettify where

data Doc = Empty
         | Char Char
         | Text String
         | Line
         | Concat Doc Doc
         | Union Doc Doc
           deriving (Show, Eq)

empty = Empty

char c = Char c

text "" = Empty
text s = Text s

double d = text (show d)

line = Line

Empty <> y = y
x <> Empty = x
x <> y = x `Concat` y

hcat = fold (<>)

fold f = foldr f empty

fsep = fold (</>)

x </> y = x <> softline <> y

softline = group line

group x = flatten x `Union` x

flatten (x `Concat` y) = flatten x `Concat` flatten y
flatten Line           = Char ' '
flatten (x `Union` _)  = flatten x
flatten other          = other
