lastButOne xs = take 1 (drop (length xs - 2) xs)

type A = String

data Test = Test {
  a :: A
                 }
  deriving (Show)
