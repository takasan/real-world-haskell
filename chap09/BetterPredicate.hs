module BetterPredicate where

{-# OPTIONS_GHC -cpp #-}
import Control.Monad
import System.Directory (Permissions(..), getModificationTime, getPermissions, getDirectoryContents)
import System.FilePath (takeExtension, (</>))
import Control.Exception
import System.IO (IOMode(..), hClose, hFileSize, openFile)
import Data.List
import System.Time

import RecursiveContents (getRecursiveContents)

type Predicate =  FilePath
               -> Permissions
               -> Maybe Integer
               -> ClockTime
               -> Bool

getFileSize :: FilePath -> IO (Maybe Integer)
getFileSize path = handle (\e -> do
                              putStrLn (show (e :: IOException))
                              return Nothing) $
                   bracket (openFile path ReadMode) hClose $ \h -> do
                     size <- hFileSize h
                     return (Just size)

betterFile :: Predicate -> FilePath -> IO [FilePath]
betterFile p path = getRecursiveContents path >>= filterM check
  where check name = do
          perms <- getPermissions name
          size <- getFileSize name
          modified <- getModificationTime name
          return (p name perms size modified)

simpleFileSize :: FilePath -> IO Integer
simpleFileSize path = do
  h <- openFile path ReadMode
  size <- hFileSize h
  hClose h
  return size

type InfoP a =  FilePath
             -> Permissions
             -> Maybe Integer
             -> ClockTime
             -> a

pathP :: InfoP FilePath
pathP path _ _ _ = path

sizeP :: InfoP Integer
sizeP _ _ (Just size) _ = size
sizeP _ _ Nothing     _ = -1

equalP :: (Eq a) => InfoP a -> a -> InfoP Bool
equalP f k = \w x y z -> f w x y z == k

liftP :: (a -> b -> c) -> InfoP a -> b -> InfoP c
liftP q f k w x y z = f w x y z `q` k

greaterP :: (Ord a) => InfoP a -> a -> InfoP Bool
greaterP = liftP (>)
lesserP :: (Ord a) => InfoP a -> a -> InfoP Bool
lesserP = liftP (<)

andP = liftP (&&)
orP = liftP (||)

(==?) :: (Eq a) => InfoP a -> a -> InfoP Bool      
(==?) = equalP
(&&?) = andP
(||?) = orP
(>?) :: (Ord a) => InfoP a -> a -> InfoP Bool      
(>?) = greaterP
(<?) :: (Ord a) => InfoP a -> a -> InfoP Bool      
(<?) = lesserP

infix 4 ==?
infix 3 &&?
infix 2 ||?
infix 4 >?
infix 4 <?

data Info = Info {
    infoPath :: FilePath
  , infoPerms :: Maybe Permissions
  , infoSize :: Maybe Integer
  , infoModTime :: Maybe ClockTime
  } deriving (Eq, Ord, Show)

getInfo :: FilePath -> IO Info
getInfo path = do
  perms <- maybeIO (getPermissions path)
  size <- maybeIO (bracket (openFile path ReadMode) hClose hFileSize)
  modified <- maybeIO (getModificationTime path)
  return (Info path perms size modified)

maybeIO :: IO a -> IO (Maybe a)
maybeIO act = handle (\e -> do
                         let s = e :: SomeException
                         return Nothing) (Just `liftM` act)

traverse :: ([Info] -> [Info]) -> FilePath -> IO [Info]
traverse order path = do
  names <- getUsefulContents path
  contents <- mapM getInfo (path : map (path </>) names)
  liftM concat $ forM (order contents) $ \info -> do
    if isDirectory info && infoPath info /= path
      then traverse order (infoPath info)
      else return [info]

getUsefulContents :: FilePath -> IO [String]
getUsefulContents path = do
  names <- getDirectoryContents path
  return (filter (`notElem` [".",".."]) names)

isDirectory :: Info -> Bool
isDirectory = maybe False searchable . infoPerms

comp :: Info -> Info -> Ordering
comp a b = if infoPath b < infoPath a
           then LT
           else if infoPath b > infoPath a
                then GT
                else EQ

type InfoPredicate =  Info
                   -> Bool

size :: Info -> Integer
size (Info _ _ (Just s) _) = s
size (Info _ _ Nothing  _) = -1

lift :: (a -> b -> c) -> (Info -> a) -> b -> Info -> c
lift f a b info = (a info) `f` b

(==??) :: (Eq a) => (Info -> a) -> a -> Info -> Bool
(==??) = lift (==)

(&&??) = lift (&&)

(||??) = lift (||)

(>??) :: (Ord a) => (Info -> a) -> a -> Info -> Bool
(>??) = lift (>)

(<??) :: (Ord a) => (Info -> a) -> a -> Info -> Bool
(<??) = lift (<)

liftPath :: (FilePath -> a) -> Info -> a
liftPath f (Info path _ _ _) = f path

wrapper :: ([Info] -> [Info]) -> (Info -> Bool) -> FilePath -> IO [Info]
wrapper order f path = do
  names <- traverse order path
  return (filter f names)
