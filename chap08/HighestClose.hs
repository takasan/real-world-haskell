import qualified Data.ByteString.Lazy.Char8 as L

readPrice :: L.ByteString -> Maybe Int
readPrice str = case L.readInt str of
  Nothing -> Nothing
  Just (dollar,rest) ->
    case L.readInt (L.tail rest) of
      Nothing -> Nothing
      Just (cents,more) ->
        Just (dollar * 100 + cents)

closing = readPrice . (!!4) . L.split ','

highestClose = maximum . (Nothing:) . map closing . L.lines

highestCloseFrom path = do
  contents <- L.readFile path
  print (highestClose contents)

class Test a where
  (=/=) :: Bool -> Bool -> a

instance Test Bool where
  a =/= b = True

instance Test Int where
  a =/= b = 0
