module GlobRegex_either
       (
         globToRegex
       , matchesGlob
       , GlobError
       ) where
import Text.Regex.Posix
import Data.Char

type GlobError = String

globToRegex :: String -> Either GlobError String
globToRegex cs = case result of
  (Right s) -> Right ('^' : s ++ "$")
  (Left s)  -> Left s
  where result = globToRegex' cs
                 
globToRegex' "" = Right ""
globToRegex' ('*':cs) = case result of
  (Right s) -> Right (".*" ++ s)
  (Left s)  -> Left s
  where result = globToRegex' cs
  
globToRegex' ('?':cs) = case result of
  (Right s) -> Right ('.' : s)
  (Left s)  -> Left s
  where result = globToRegex' cs

globToRegex' ('[':'!':c:cs) = case result of
  (Right s) -> Right ("[^" ++ c : s)
  (Left s)  -> Left s
  where result = charClass cs
  
globToRegex' ('[':c:cs) = case result of
  (Right s) -> Right ('[' : c : s)
  (Left s)  -> Left s
  where result = charClass cs
  
globToRegex' ('[':_) = Left "unterminated character class"
globToRegex' (c:cs) = case result of
  (Right s) -> Right (escape c ++ s)
  (Left s)  -> Left s
  where result = globToRegex' cs

escape :: Char -> String
escape c | c `elem` regexChars = '\\' : [c]
         | otherwise = [c]
  where regexChars = "\\+()^$.{}]|"

charClass :: String -> Either GlobError String
charClass (']':cs) = case result of
  (Right s) -> Right (']' : s)
  (Left s)  -> Left s
  where result = globToRegex' cs
  
charClass (c:cs) = case result of
  (Right s) -> Right (c : s)
  (Left s)  -> Left s
  where result = charClass cs
charClass [] = Left "unterminated character class"

matchesGlob :: FilePath -> String -> Either GlobError Bool
name `matchesGlob` pat = case regex of
  (Right s) -> Right (name =~ s)
  (Left s)  -> Left s
  where regex = globToRegex pat
