module Glob (namesMatching) where

import System.Directory (doesDirectoryExist, doesFileExist,
                         getCurrentDirectory, getDirectoryContents)
import System.FilePath (dropTrailingPathSeparator, splitFileName, (</>),pathSeparator)
import Control.Exception
import Control.Monad (forM,filterM)
import GlobRegex (matchesGlob,myMatchesGlob)
import Data.List

isPattern = any (`elem` "[*?")

namesMatching pat
  | not (isPattern pat) = do
    exists <- doesNameExist pat
    return (if exists then [pat] else [])
  | otherwise = do
    case splitFileName pat of
      ("",baseName) -> do
        curDir <- getCurrentDirectory
        listMatches curDir baseName
      (dirName,baseName) -> do
        dirs <- if isPattern dirName
                then namesMatching (dropTrailingPathSeparator dirName)
                else return [dirName]
        let listDir = if isPattern baseName
                      then listMatches
                      else listPlain
        pathNames <- forM dirs $ \dir -> do
          baseNames <- listDir dir baseName
          return (map (dir </>) baseNames)
        return (concat pathNames)

doesNameExist :: FilePath -> IO Bool
doesNameExist name = do
  fileExists <- doesFileExist name
  if fileExists
    then return True
    else doesDirectoryExist name

listMatches :: FilePath -> String -> IO [String]
listMatches dirName pat = do
  dirName' <- if null dirName
              then getCurrentDirectory
              else return dirName
  handle ignore $ do
    names <- if "**" `isInfixOf` pat
             then getDirectoryRecursive dirName'
             else getDirectoryContents dirName'
    let names' = if isHidden pat
                 then filter isHidden names
                 else filter (not . isHidden) names
    return (filter (\name -> myMatchesGlob name pat isWindows) names')
  where isWindows = pathSeparator == '\\'

isHidden ('.':_) = True
isHidden _ = False

ignore :: SomeException -> IO [String]
ignore _ = return []

listPlain :: FilePath -> String -> IO [String]
listPlain dirName baseName = do
  exists <- if null baseName
            then doesDirectoryExist dirName
            else doesNameExist (dirName </> baseName)
  return (if exists then [baseName] else [])

getDirectoryRecursive :: FilePath -> IO [String]
getDirectoryRecursive dirName = do
  names <- getDirectoryContents dirName
  names' <- filterM (\x -> return (x /= "." && x /= "..")) names
  dirs <- filterM (\x -> doesDirectoryExist (dirName </> x)) names'
  files <- filterM (\x -> doesFileExist (dirName </> x)) names'
  n <- mapM (\x -> getDirectoryRecursive (dirName </> x)) dirs
  return ((concat n) ++ (map (\x -> dirName </> x) files))

--  (String -> IO [String]) -> [String] -> IO [[String]]
