module GlobRegex
       (
         globToRegex
       , matchesGlob
       , myMatchesGlob
       ) where
import Text.Regex.Posix
import Data.Char

globToRegex :: String -> String
globToRegex cs = '^' : globToRegex' cs ++ "$"
                 
globToRegex' "" = ""
globToRegex' ('*':cs) = ".*" ++ globToRegex' cs
globToRegex' ('?':cs) = '.' : globToRegex' cs
globToRegex' ('[':'!':c:cs) = "[^" ++ c : charClass cs
globToRegex' ('[':c:cs) = '[' : c : charClass cs
globToRegex' ('[':_) = error "unterminated character class"
globToRegex' (c:cs) = escape c ++ globToRegex' cs

escape :: Char -> String
escape c | c `elem` regexChars = '\\' : [c]
         | otherwise = [c]
  where regexChars = "\\+()^$.{}]|"

charClass :: String -> String
charClass (']':cs) = ']' : globToRegex' cs
charClass (c:cs) = c : charClass cs
charClass [] = error "unterminated character class"

matchesGlob :: FilePath -> String -> Bool
name `matchesGlob` pat = name =~ globToRegex pat

myMatchesGlob name pat iFlag = name =~ myGlobToRegex pat iFlag

myGlobToRegex cs iFlag = '^' : myGlobToRegex' cs ++ "$"
  where myGlobToRegex' "" = ""
        myGlobToRegex' ('*':cs) = ".*" ++ myGlobToRegex' cs
        myGlobToRegex' ('?':cs) = '.' : myGlobToRegex' cs
        myGlobToRegex' ('[':'!':c:cs) = "[^" ++ myChar c "" "" ++ myCharClass cs
        myGlobToRegex' ('[':c:cs) = '[' : myChar c "" "" ++ myCharClass cs
        myGlobToRegex' ('[':_) = error "unterminated character class"
        myGlobToRegex' (c:cs) = myEscape c ++ myGlobToRegex' cs

        myCharClass (']':cs) = ']' : myGlobToRegex' cs
        myCharClass (c:cs) = myChar c "" "" ++ myCharClass cs
        myCharClass [] = error "unterminated character class"

        myEscape c | c `elem` regexChars = '\\' : [c]
                   | otherwise = myChar c "[" "]"
          where regexChars = "\\+()^$.{}]|"

        myChar c open close | iFlag && c >= 'a' && c <= 'z' = open ++ [c] ++ [toUpper c] ++ close
                            | iFlag && c >= 'A' && c <= 'Z' = open ++ [toLower c] ++ [c] ++ close
                            | otherwise = [c]
                        
