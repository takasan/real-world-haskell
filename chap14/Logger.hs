
import System.Random

type Log = [String]

data Logger a = Logger (a, Log)
                deriving (Show)

record :: String -> Logger ()
record s = Logger ((), [s])

runLogger (Logger a) = a

test :: Logger a -> (a -> Logger b) -> Logger b
test (Logger xs@(a, log)) f = f a

instance Monad Logger where
  (Logger xs@(a, log)) >>= f =
    case f a of
      (Logger ys@(b, log')) -> Logger (b, log' ++ log)
  a >> b = a >>= (\_ -> b)
  return a = Logger (a, [])
  fail = error

globToRegex' :: String -> Logger String
globToRegex' "" = return "$"
globToRegex' ('?':cs) =
  record "any" >>
  globToRegex' cs >>= \ds ->
  return ('.':ds)
globToRegex' (c:cs) =
  globToRegex' cs >>= \ds ->
  return (c:ds)
