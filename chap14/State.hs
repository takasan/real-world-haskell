
module State where

newtype State s a = State {
  runState :: s -> (a, s)
  }

instance Monad (State a) where
  return m = State $ \s -> (m, s)
  m >>= k  = State $ \s -> let (a, s') = runState m s
                           in runState (k a) s'

get :: State s s
get = State $ \s -> (s, s)

put :: s -> State s ()
put s = State $ \_ -> ((), s)
