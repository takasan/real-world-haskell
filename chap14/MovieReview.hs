
import Control.Monad

data MovieReview = MovieReview {
    revTitle :: String
  , revUser :: String
  , revReview :: String
  }

lookup1 key alist = case lookup key alist of
  Just (Just a@(_:_)) -> Just a
  _ -> Nothing

test alist = (MovieReview `liftM` lookup1 "title" alist) $ lookup1 "user" alist
